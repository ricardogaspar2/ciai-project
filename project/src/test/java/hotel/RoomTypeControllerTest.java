package hotel;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Collection;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import app.Application;
import app.model.Hotel;
import app.model.Room;
import app.model.RoomType;
import app.repository.HotelRepository;
import app.repository.RoomRepository;
import app.repository.RoomTypeRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebAppConfiguration
public class RoomTypeControllerTest {

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@Autowired
	HotelRepository hotels;

	@Autowired
	RoomTypeRepository roomtypes;

	@Autowired
	RoomRepository rooms;

	private TestData testData;

	@Before
	public void setUp() {

		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();

		testData = new TestData();

		// init Hotels repo
		for (int i = 0; i < testData.testHotels.length; i++) {
			hotels.save(testData.testHotels[i]);
		}

		// init roomTypes
		for (int i = 0; i < testData.testRoomTypes.length; i++) {
			roomtypes.save(testData.testRoomTypes[i]);
		}

		// init rooms
		for (Room testRoom : testData.testRooms) {
			rooms.save(testRoom);
		}
	}

	@Test
	public void testIndex() throws Exception {
		mvc.perform(get("/roomtypes")).andExpect(status().isOk()).andExpect(view().name("roomtypes/index"));
	}

	@Test
	public void testIndexByHotel() throws Exception {
		Hotel testHotel = testData.testHotels[0];
		mvc.perform(get("/hotels/" + testHotel.getId() + "/roomtypes/")).andExpect(status().isOk())
				.andExpect(view().name("roomtypes/index"));
	}

	@Test
	public void testShow() throws Exception {
		RoomType testRoomType = testData.testRoomTypes[0];
		mvc.perform(get("/roomtypes/" + testRoomType.getId())).andExpect(status().isOk())
				.andExpect(view().name("roomtypes/show"));
	}

	@Test
	public void testShowUsingHotel() throws Exception {
		Hotel testHotel = testData.testHotels[0];
		RoomType testRoomType = testData.testRoomTypes[0];
		mvc.perform(get("/hotels/" + testHotel.getId() + "/roomtypes/" + testRoomType.getId()))
				.andExpect(status().isOk()).andExpect(view().name("roomtypes/show"));
	}

	@Test
	public void testNewRoomTypeUsingHotel() throws Exception {
		Hotel testHotel = testData.testHotels[2];
		RoomType testRoomType = new RoomType("Triple", testHotel);

		mvc.perform(post("/hotels/" + testHotel.getId() + "/roomtypes/new").param("type", testRoomType.getType())
		// .param("hotel", ((Long)testHotel.getId()).toString())
		).andExpect(status().isOk()).andExpect(view().name("roomtypes/show"));

		Collection<RoomType> resultRoomTypes = roomtypes.findByHotelId(testHotel.getId());
		Iterator<RoomType> it = resultRoomTypes.iterator();
		while (it.hasNext()) {
			RoomType roomType = (RoomType) it.next();
			System.out.println(roomType);

		}

		// assertTrue(hotel != null);
		// assertTrue(hotel.name.equals(hotelName));
		// assertTrue(hotel.address.equalsIgnoreCase(hotelAddress));
		// assertTrue(hotel.category.equals(hotelCategory));
		// assertTrue(hotel.stars==Integer.parseInt(stars));
	}

}