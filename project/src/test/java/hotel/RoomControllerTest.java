package hotel;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import app.Application;
import app.model.Hotel;
import app.model.Room;
import app.model.RoomType;
import app.repository.HotelRepository;
import app.repository.RoomRepository;
import app.repository.RoomTypeRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebAppConfiguration
public class RoomControllerTest {

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@Autowired
	HotelRepository hotels;

	@Autowired
	RoomTypeRepository roomtypes;
	
	@Autowired
	RoomRepository rooms;
	
	private TestData testData;
	
	
	
	@Before
	public void setUp() {

		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
		
		testData = new TestData();
		
		// init Hotels repo
		for (int i = 0; i < testData.testHotels.length; i++) {
			hotels.save(testData.testHotels[i]);
		}

		// init roomTypes
		for (int i = 0; i < testData.testRoomTypes.length; i++) {
			roomtypes.save(testData.testRoomTypes[i]);
		}
		
		// init rooms
		for (Room testRoom : testData.testRooms) {		
			rooms.save(testRoom);
		}
	

		

	}

	@Test
	public void testIndex() throws Exception {
		mvc.perform(get("/rooms")).andExpect(status().isOk()).andExpect(view().name("rooms/index"));
	}
	
}
