package hotel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import app.Application;
import app.controllers.HotelController;
import app.model.Hotel;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Iterator;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import app.repository.HotelRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebAppConfiguration
public class HotelControllerTest {
	
	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;
	
	@Autowired
	HotelRepository hotels;

	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}
	
	
	@Test
	public void testIndex() throws Exception {
		mvc.perform(get("/hotels")).andExpect(status().isOk())
				.andExpect(view().name("hotels/index"));
	}


	
	
	@Test
	public void testAddHotel() throws Exception {
		String hotelName 	 = "Salgados";
		String hotelAddress  = "Avenida da Liberdade"; 
		String hotelCategory = "Business";
		String stars = "4";
		mvc.perform(post("/hotels")
				.param("address"	, hotelAddress)
                .param("name"		, hotelName)
				.param("category"	, hotelCategory)
				.param("stars"	, stars))
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("/hotels"));;
			
		Hotel hotel = hotels.findByName(hotelName);
		
//		assertTrue(hotel != null);
//		assertTrue(hotel.name.equals(hotelName));
//		assertTrue(hotel.address.equalsIgnoreCase(hotelAddress));
//		assertTrue(hotel.category.equals(hotelCategory));
//		assertTrue(hotel.stars==Integer.parseInt(stars));
		
		assertTrue(hotel.getName().equals(hotelName));
		assertTrue(hotel.getAddress().equalsIgnoreCase(hotelAddress));
		assertTrue(hotel.getCategory().equals(hotelCategory));
		assertTrue(hotel.getAddress().equalsIgnoreCase(hotelAddress));
//		assertTrue(hotel.getStars()==Integer.parseInt(stars));
		assertTrue(hotel.getStars().equalsIgnoreCase(stars));
	}
	
	@Test
	public void testGetOne() throws Exception {
		String hotelName = "Marriot"; 

		Hotel hotel = hotels.findByName(hotelName);
		

		mvc.perform(get("/hotels/"+hotel.getId()))
				.andExpect(view().name("hotels/show"));
	
		hotelName = "Intercontinental"; 

		hotel = hotels.findByName(hotelName);
		
		mvc.perform(get("/hotels/"+hotel.getId()))
				.andExpect(view().name("hotels/show"));
	}
	
	@Test
	public void testModel() throws Exception {
		mvc.perform(get("/hotels"))
				.andExpect(model().attributeExists("hotels"));
	}
}
