package hotel;

import java.util.ArrayList;
import java.util.Date;

import app.model.Booking;
import app.model.Hotel;
import app.model.Room;
import app.model.RoomType;

public class TestData {

	// hotels to Test
	protected Hotel testHotels[] = { new Hotel("Ibis Bonga", "Rua Bonga", "Hotel", "1"),
			new Hotel("Xinguila", "Rua X", "Hostel", "2"), new Hotel("Maravliha", "Avenida do Mar", "Hostel", "3"),
			new Hotel("Holiday Inn Taguspark", "Rua do Tagus", "Hotel", "4"),
			new Hotel("Novotel Tavira", "Rua do Mar", "Hotel", "5") };

	// room types to test
	protected RoomType testRoomTypes[] = { new RoomType("Suite"), new RoomType("Single"), new RoomType("Double") };

	protected ArrayList<Room> testRooms = fillRooms();

	protected Booking testBookings[] = { new Booking("2015/08/29", "2015/08/31", "my weekend off", 2),
			new Booking("2015/10/08", "2015/11/08", "dream trip", 4),
			new Booking("2015/07/02", "2015/07/20", "summer vacations", 7),
			new Booking("2015/12/21", "2015/12/29", "christmas hollidays", 8) };

	private ArrayList<Room> fillRooms() {
		ArrayList<Room> rooms = new ArrayList<>();

		for (Hotel hotel : testHotels) {
			// suites
			for (int i = 0; i < 5; i++) {

				rooms.add(new Room(3, 1, 90f, testRoomTypes[0], hotel));
			}
			// single Rooms
			for (int i = 0; i < 5; i++) {
				rooms.add(new Room(2, i, 60f, testRoomTypes[1], hotel));
			}

			// single Rooms
			for (int i = 0; i < 5; i++) {
				rooms.add(new Room(1, i, 30f, testRoomTypes[1], hotel));
			}

		}
		return rooms;
	}

}
