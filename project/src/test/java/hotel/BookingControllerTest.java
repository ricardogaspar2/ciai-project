package hotel;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import app.Application;
import app.repository.HotelRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
@WebAppConfiguration
public class BookingControllerTest {

	@Autowired
	private WebApplicationContext context;

	private MockMvc mvc;

	@Autowired
	HotelRepository hotels;
	
	TestData testData = new TestData();

	@Before
	public void setUp() {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	@Test
	public void testIndex() throws Exception {
		mvc.perform(get("/bookings")).andExpect(status().isOk()).andExpect(view().name("bookings/index"));
	}

	@Test
	public void testNewBooking() throws Exception {
		mvc.perform(post("/bookings/new")
				.param("startDate", "2015/08/20")
				.param("endDate", "2015/09/20")
				.param("numberOfPersons", "2")
				.param("description", "month on vaction")
				.param("hotelid",testData.testHotels[0].getId()+"")
				.param("roomtypeID", testData.testRoomTypes[2].getId()+""))
		.andExpect(status().isOk())
				.andExpect(view().name("bookings/"));
	}
	
	@Test
	public void testBookingTimeInterval() throws Exception {
		String startDate = "2015/08/20";
		String endDate = "2015/09/20";
		mvc.perform(get("/bookings?startdate="+startDate+"&enddate="+endDate))
		.andExpect(status().isOk())
				.andExpect(view().name("bookings/"));
	}
}
