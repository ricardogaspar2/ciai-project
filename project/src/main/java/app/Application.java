package app;

import java.sql.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;

import app.model.Booking;
import app.model.Comment;
import app.model.Hotel;
import app.model.RoomType;
import app.repository.BookingRepository;
import app.repository.CommentRepository;
import app.repository.HotelRepository;
import app.repository.RoomTypeRepository;
import app.repository.RoomRepository;

@SpringBootApplication
public class Application implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Application.class);
	
	/**
	 * The main() method uses Spring Boot’s SpringApplication.run() method to launch an application.
	 * The run() method returns an ApplicationContext where all the beans that were created 
	 * either by your app or automatically added thanks to Spring Boot are.
	 * @param args
	 */
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    
    @Autowired
    HotelRepository hotels;

    @Autowired
    RoomTypeRepository roomtypes;
    
    @Autowired
    RoomRepository room;
    
    @Autowired
    CommentRepository comments;
    @Autowired
    BookingRepository bookings;

    @Override
    @Transactional
    public void run(String... strings) {
    	    
    	log.info("Setting up seed data");
    	
    	//Hotel(String name, String address, String category, int stars)
    	hotels.deleteAll();
    	Hotel myHotels[] = {new Hotel("Marriot", "Rua do Sul", "Hotel", "5"), 
    						new Hotel("Intercontinental", "Rua do Norte", "Hostel", "2"), 
    						new Hotel("Trip", "Rua do Este", "Hostel", "2"), 
    						new Hotel("Holiday Inn", "Rua do Oeste", "Hotel", "4"), 
    						new Hotel("Tulip", "Rua do Nordeste", "Hostel", "3"), 
    						new Hotel("Hostel da Costa", "Rua do Cha", "Hotel", "5")
    						};
    	
    	for(Hotel hotel : myHotels) hotels.save(hotel);

    	
    	Hotel marriot = myHotels[0];
    	Hotel intercontinental = myHotels[1];
    	
 
    	RoomType[] myRoomTypes = {new RoomType("Single", marriot), 
    						new RoomType("Double", marriot),
    						new RoomType("Single", intercontinental), 
    						new RoomType("Double", intercontinental),
    						};
    	
    	Comment[] myComments ={new Comment("Uma Bela ...", new Date(1445210029790L), marriot),
    						new Comment("Não Gostei!!", new Date(1445210038790L), marriot),
    						new Comment("Não Gostei!!", new Date(1445210039790L), intercontinental),
    						};
    	
    	Booking myBookings[]= {
    			new Booking("2015/11/06","2015/11/08", "my weekend off",2, marriot),
    			new Booking("2016/04/20","2016/04/25", "Tomorrowland", 5, intercontinental),
    			new Booking("2016/02/08","2016/02/12", "Carnival",4, marriot),
    			new Booking("2016/12/20","2015/12/31", "christmas & new years eve",6, intercontinental)
    	};
    	
    	
    	marriot.getRoomTypes().add(myRoomTypes[0]);
    	marriot.getRoomTypes().add(myRoomTypes[1]);
    	
    	marriot.getComments().add(myComments[0]);
    	marriot.getComments().add(myComments[1]);
    	
    	marriot.getBookings().add(myBookings[0]);
    	marriot.getBookings().add(myBookings[2]);
    	
    	
    	intercontinental.getRoomTypes().add(myRoomTypes[2]);
    	intercontinental.getRoomTypes().add(myRoomTypes[3]);
    	
    	intercontinental.getComments().add(myComments[2]);

    	intercontinental.getBookings().add(myBookings[1]);
    	intercontinental.getBookings().add(myBookings[3]);
    	
    	
//    	for(Comment comment: myComments) comments.save(comment);
    	
//    	for(RoomType type:types) roomtypes.save(type);
    	
    	for(Hotel hotel : myHotels) hotels.save(hotel);

//		Room[] roomArray = {new Room(12,121,types[0]), 
//							new Room(12,122,types[1]), 
//							new Room(13,131,types[2]), 
//							new Room(13,132,types[3])
//							};		
//		
//		for(Room roomAux:roomArray) room.save(roomAux);
    }
}


