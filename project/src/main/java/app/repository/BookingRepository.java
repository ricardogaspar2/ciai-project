package app.repository;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

import app.model.Booking;

public interface BookingRepository extends CrudRepository<Booking, Long> {

	
	Collection<Booking> findByHotelId(long hotelid);
	
	
}
