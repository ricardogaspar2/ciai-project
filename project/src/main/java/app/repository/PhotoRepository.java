package app.repository;


import org.springframework.data.repository.CrudRepository;

import app.model.Photo;


public interface PhotoRepository extends CrudRepository<Photo, Long> {

}
