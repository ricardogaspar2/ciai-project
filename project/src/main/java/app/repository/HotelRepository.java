package app.repository;

import org.springframework.data.repository.CrudRepository;

import app.model.Hotel;


public interface HotelRepository extends CrudRepository<Hotel, Long> {
	
	Hotel findByName(String name);
	
}

