package app.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import app.model.Booking;
import app.model.Room;


public interface RoomRepository extends CrudRepository<Room, Long> {
	
	@Query(value = "SELECT * FROM HOTELS, ROOM_TYPES, ROOMS "
			+ "where ROOM_TYPES.ROOMTYPES_HOTELS_ID = ?1 and "
			+ "ROOM_TYPES.ROOMTYPES_ID = ROOMS.ROOMS_ROOMTYPES_ID",
			nativeQuery = true)
	Collection<Booking> findByHotel(@Param("hotelid") long hotelid);
}

