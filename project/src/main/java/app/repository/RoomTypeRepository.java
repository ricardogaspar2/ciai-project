package app.repository;


import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

import app.model.RoomType;


public interface RoomTypeRepository extends CrudRepository<RoomType, Long> {
	Collection<RoomType> findByHotelId(long id);
	Collection<RoomType> findByHotelName(String name);
	
}

