package app.repository;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;

import app.model.Comment;


public interface CommentRepository extends CrudRepository<Comment, Long> {
	
//	Collection<Comment> findByHotelId(long id);

}
