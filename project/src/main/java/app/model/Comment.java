package app.model;

import java.util.Date;

//import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "COMMENTS")
public class Comment {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	// @Column(name="COMMENTS_ID")
	private long id;

	// @Column(name="COMMENTS_COMMENT_STRING")
	private String commentString;

	// @Column(name="COMMENTS_COMMENT_DATE")
	@DateTimeFormat(pattern = "yyyy/MM/dd hh:mm:ss a")
	private Date commentDate;

	/**
	 * <code>true</code> means APPROVED, <code>false</code> means UNAPRROVED =
	 * TO BE APPROVED.
	 */
	private boolean approvalState;

	@ManyToOne
	private Hotel hotel;

	public Comment() {
		this.commentDate = new Date();
	}

	public Comment(String commentString, Date commentDate, Hotel hotel) {
		this.commentString = commentString;
		this.commentDate = commentDate;
		this.commentDate = new Date();
		this.approvalState = false;

		this.hotel = hotel;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCommentString() {
		return commentString;
	}

	public void setCommentString(String commentString) {
		this.commentString = commentString;
	}

	public Date getCommentDate() {
		return commentDate;
	}

	public void setCommentDate(Date commentDate) {
		this.commentDate = commentDate;
	}

	public boolean isApprovalState() {
		return approvalState;
	}

	public void setApprovalState(boolean approvalState) {
		this.approvalState = approvalState;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

}
