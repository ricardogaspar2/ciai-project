package app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PHOTOS")
public class Photo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="PHOTOS_ID", insertable=true, updatable=true, unique=true, nullable=false)
	// @Column(name="PHOTOS_ID")
	private long id;
	// @Column(name="PHOTOS_PATH")
	private String path;
	// @Column(name="PHOTOS_TYPE")
	private String type;

	// Foreign keys
	// private User author;

	public Photo() {
	}

	public Photo(String path, String type) {
		this.path = path;
		this.type = type;

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Id: " + getId() + " Path: " + getPath() + " Type: " + getType();
	}

}
