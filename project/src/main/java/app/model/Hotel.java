package app.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "HOTELS")
public class Hotel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	// @Column(name="HOTELS_ID")
	private long id;
	// @Column(name="HOTELS_NAME")
	private String name;
	// @Column(name="HOTELS_ADDRESS")
	private String address;
	// @Column(name="HOTELS_CATEGORY")
	private String category;
	// @Column(name="HOTELS_STARS")
	private String stars;

	/**
	 * <code>true</code> means APPROVED, <code>false</code> means UNAPRROVED =
	 * TO BE APPROVED.
	 */
	private boolean approvalState;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "hotel", orphanRemoval = true)
	// @Column(name="HOTELS_ROOMTYPES_ID")
	private Collection<RoomType> roomTypes = new ArrayList<RoomType>();

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "hotel", orphanRemoval = true)
	// @Column(name="HOTELS_ROOMS_ID")
	private Collection<Room> rooms = new ArrayList<Room>();

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "hotel", orphanRemoval = true)
	// @Column(name="HOTELS_BOOKINGS_ID")
	private Collection<Booking> bookings = new ArrayList<Booking>();

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	// @Column(name="HOTELS_PHOTOS_ID")
	private Collection<Photo> photos = new ArrayList<Photo>();

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	// @Column(name="HOTELS_COMMENTS_ID")
	private Collection<Comment> comments = new ArrayList<Comment>();

	public Hotel() {
	}

	public Hotel(String name, String address, String category, String stars) {
		this.name = name;
		this.address = address;
		this.category = category;
		this.stars = stars;

		this.approvalState = false;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getStars() {
		return stars;
	}

	public void setStars(String stars) {
		this.stars = stars;
	}

	public boolean isApprovalState() {
		return approvalState;
	}

	public void setApprovalState(boolean approvalState) {
		this.approvalState = approvalState;
	}

	public Collection<RoomType> getRoomTypes() {
		return roomTypes;
	}

	public void setRoomTypes(Collection<RoomType> roomTypes) {
		this.roomTypes = roomTypes;
	}

	public Collection<Room> getRooms() {
		return rooms;
	}

	public void setRooms(Collection<Room> rooms) {
		this.rooms = rooms;
	}

	public Collection<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(Collection<Booking> bookings) {
		this.bookings = bookings;
	}

	public Collection<Photo> getPhotos() {
		return photos;
	}

	public void setPhotos(Collection<Photo> photos) {
		this.photos = photos;
	}

	public Collection<Comment> getComments() {
		return comments;
	}

	public void setComments(Collection<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "Hotel [id=" + id + ", name=" + name + ", address=" + address + ", category=" + category + ", stars="
				+ stars + "]";
	}
}
