package app.model;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "BOOKINGS")
public class Booking {
	
	private static final String DATE_FORMAT= "yyyy/MM/dd";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	// @Column(name="BOOKINGS_ID")
	private long id;
	// @Column(name="BOOKINGS_START_DATE")
	@DateTimeFormat(pattern = DATE_FORMAT)
	private Date startDate;
	// @Column(name="BOOKINGS_END_DATE")
	@DateTimeFormat(pattern = DATE_FORMAT)
	private Date endDate;
	// @Column(name="BOOKINGS_DESCRIPTION")
	private String description;
	
	private int numberOfPersons;

	/**
	 * <code>true</code> means APPROVED, <code>false</code> means UNAPRROVED =
	 * TO BE APPROVED.
	 */
	private boolean approvalState;

	@ManyToOne
	private Hotel hotel;

	@ManyToMany
	private Collection<Room> rooms;

	public Booking() {
	}

	public Booking(String startDate, String endDate, String description, int numberOfPersons) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

		try {
			this.startDate = dateFormat.parse(startDate);
			this.endDate = dateFormat.parse(endDate);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.description = description;
		this.numberOfPersons = numberOfPersons;
		this.approvalState = false;
	}
	
	public Booking(String startDate, String endDate, String description, int numberOfPersons, Hotel hotel) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);

		try {
			this.startDate = dateFormat.parse(startDate);
			this.endDate = dateFormat.parse(endDate);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.description = description;
		this.numberOfPersons = numberOfPersons;
		this.approvalState = false;

		 this.hotel = hotel;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getNumberOfPersons() {
		return numberOfPersons;
	}

	public void setNumberOfPersons(int numberOfPersons) {
		this.numberOfPersons = numberOfPersons;
	}

	public boolean isApprovalState() {
		return approvalState;
	}

	public void setApprovalState(boolean approvalState) {
		this.approvalState = approvalState;
	}

	public Hotel getHotel() {
		return hotel;
	}
	
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Collection<Room> getRooms() {
		return rooms;
	}

	public void setRooms(Collection<Room> rooms) {
		this.rooms = rooms;
	}

}
