package app.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "ROOMTYPES")
public class RoomType {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
//    @Column(name="ROOMTYPES_ID")
    private long id;
//    @Column(name="ROOMTYPES_TYPE")
    private String type;
    
    @ManyToOne
    private Hotel hotel;
    

//	@OneToMany(cascade={CascadeType.ALL}, mappedBy="roomType")
//	@Column(name="ROOMTYPES_ROOMS_ID")
//    private Collection<Room> rooms = new ArrayList<Room>();
    
    
    public RoomType() {}
    
    
    public RoomType(String type) {
    	this.type = type;
    }
    
    public RoomType(String type, Hotel hotel) {
    	this.type = type;
    	this.hotel = hotel;
    }
    
    @Override
    public String toString() {
    	return "Id: " + getId() + "\n Room Type: " + getType();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Hotel getHotel() {
		return hotel;
	}
	
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	
	
}

