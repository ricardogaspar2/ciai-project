package app.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ROOMS")
public class Room {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
//    @Column(name="ROOMS_ID")
    private long id;
//    @Column(name="ROOMS_FLOOR")
    private int floor;
//    @Column(name="ROOMS_NUMBER")
    private int number;   
//  @Column(name="ROOMS_PRICE")
    private float price;
    
    @ManyToOne
    private Hotel hotel;
    
    @ManyToOne
    private RoomType roomType;

    @ManyToMany(cascade=CascadeType.ALL, mappedBy="rooms")
//    @Column(name="ROOMS_BOOKINGS_ID")
    private Collection<Booking> bookings = new ArrayList<Booking>();
    
    public Room() {}
    
    public Room(int floor, int roomNumber, float price, RoomType roomType, Hotel hotel) {
    	
    	this.floor = floor;
    	this.number = roomNumber;
    	this.price = price;
    	this.roomType  = roomType;
    	
    	this.hotel = hotel;
    }

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getFloor() {
		return floor;
	}

	public void setFloor(int floor) {
		this.floor = floor;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public RoomType getRoomType() {
		return roomType;
	}

	public void setRoomType(RoomType roomType) {
		this.roomType = roomType;
	}

	public Collection<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(Collection<Booking> bookings) {
		this.bookings = bookings;
	}

	@Override
	public String toString() {
		return "Room [id=" + id + ", floor=" + floor + ", number=" + number + ", roomType=" + roomType + ", bookings="
				+ bookings + "]";
	}


}

