package app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.repository.RoomRepository;


@Controller
@RequestMapping(value="/hotels/{hotelid}/roomstypes/{roomtypesid}/rooms")
public class RoomController {
	
	@Autowired
	RoomRepository rooms;
	
	// GET  /hotels/{id}/rooms 			- the list all bookings of Hotel with Id
    @RequestMapping(method=RequestMethod.GET)
    public String index(@PathVariable("hotelid") long hotelid, Model model) {
    	model.addAttribute("hotelid", hotelid); //TODO Não funciona
        model.addAttribute("rooms", rooms.findByHotel(hotelid));
    	return "rooms/index";
    }
}
