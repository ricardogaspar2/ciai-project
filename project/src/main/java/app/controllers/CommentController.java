package app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import app.model.Comment;
import app.model.Hotel;
import app.repository.CommentRepository;
import app.repository.HotelRepository;
import app.util.HotelNotFoundException;

@Controller
//@RequestMapping(value="/hotels/{hotelid}/comments")
public class CommentController {
	
	@Autowired
	CommentRepository comments;
	
	@Autowired
	HotelRepository hotels;
	
	// GET  /hotels/{id}/comments 			- the list all the comments to Hotel with Id
    @RequestMapping(value="/hotels/{hotelid}/comments", method=RequestMethod.GET)
    public String index(@PathVariable("hotelid") long hotelid, Model model) {
    	System.out.println("GET  /hotels/{id="+hotelid+"}/comments"); //TODO APAGAR
    	
    	Hotel hotel = hotels.findOne(hotelid);
    	model.addAttribute("hotelId", hotelid); 
    	model.addAttribute("comments", hotel.getComments());
        //model.addAttribute("comments", comments.findByHotelId(hotelid));
    	return "comments/index";
    }
    
	// GET  /comments 			- the list all the comments
    @RequestMapping(value="/comments", method=RequestMethod.GET)
    public String listAllComments(Model model) {
    	System.out.println("GET  /comments"); //TODO APAGAR

    	model.addAttribute("comments", comments.findAll());
    	return "comments/index";
    }
    
	// GET  /comments.json 	- the list of comments
    @RequestMapping(value="/comments",method=RequestMethod.GET, produces={"text/plain","application/json"})
    public @ResponseBody Iterable<Comment> indexJSON(Model model) {
    	System.out.println("GET  /comments.json"); //TODO APAGAR
    	
        return comments.findAll();
    }
    
 // GET  /hotels/{hotelid}/comments/{commentid}/remove 	- removes the comment with identifier {id}
    @RequestMapping(value="/hotels/{hotelid}/comments/{commentid}/remove", method=RequestMethod.GET)
    public String removeComment(@PathVariable("hotelid") long hotelid, @PathVariable("commentid") long commentid, Model model) {
    	System.out.println("GET /hotels/{hotelid="+hotelid+"}/comments/{commentid="+commentid+"}/remove"); // TODO APAGAR 
    	
    	Comment comment = comments.findOne(commentid);
    	if( comment == null )
    		throw new HotelNotFoundException();
    	
    	Hotel hotel = hotels.findOne(hotelid);
    	hotel.getComments().remove(comment);
    	hotels.save(hotel);
    	
    	return "redirect:/hotels/{hotelid}";
//    	return "redirect:/hotels/{hotelid}/comments";
    }
	
    // GET  /hotels/{id}/comments/new			- the form to fill the data for a new comment
    @RequestMapping(value="/hotels/{hotelid}/comments/new", method=RequestMethod.GET)
    public String newComment(@PathVariable("hotelid") long hotelid, Model model) {
    	System.out.println("GET  /hotels/{id="+hotelid+"}/comments/new"); // TODO APAGAR
    	
    	
    	Hotel hotel = hotels.findOne(hotelid);
    	model.addAttribute("comments", hotel.getComments());
    	
    	model.addAttribute("url", "/hotels/"+hotelid+"/comments/new");
    	Comment comment = new Comment();
    	model.addAttribute("comment", comment);
    	return "comments/form";
    }
    
    // POST /hotels/{id}/comments/new         	- creates a new hotel
    @RequestMapping(value="/hotels/{hotelid}/comments/new", method=RequestMethod.POST)
    public String saveComment(@PathVariable("hotelid") long hotelid, @ModelAttribute Comment comment, Model model) {
    	System.out.println("POST /hotels/{id="+hotelid+"}/comments/new"); // TODO APAGAR 
    	
    	Hotel hotel = hotels.findOne(hotelid);
//    	comment.setHotel(hotel);
    	hotel.getComments().add(comment);
    	//comments.save(comment);
    	hotels.save(hotel);
    	model.addAttribute("comment", comment);
//    	return "redirect:/hotels/{hotelid}/comments";
    	return "redirect:/hotels/{hotelid}";
    }
    
}
