package app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.repository.BookingRepository;;

@Controller
public class BookingController {

	@Autowired
	BookingRepository bookings;

	// GET /hotels/{id}/bookings - the list all bookings of Hotel with Id

	@RequestMapping(value = "/bookings",  method=RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("bookings", bookings.findAll());
		return "bookings/index";
	}

	@RequestMapping(value = "/hotels/{hotelid}/bookings", method=RequestMethod.GET)
	public String index(@PathVariable("hotelid") long hotelid, Model model) {
		model.addAttribute("bookings", bookings.findByHotelId(hotelid));
		return "bookings/index";
	}

}
