package app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ApplicationController {
	
	@RequestMapping(value="/")
	public String root(Model model) {
		System.out.println("GET /");
		return "/index";
	}
}
