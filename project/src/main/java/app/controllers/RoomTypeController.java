package app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import app.model.RoomType;
import app.repository.HotelRepository;
import app.repository.RoomTypeRepository;


@Controller
@SessionAttributes("roomtype")
public class RoomTypeController {

	@Autowired
	HotelRepository hotels;
	
	@Autowired
	RoomTypeRepository roomTypes;

	@RequestMapping(value="/roomtypes", method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("roomtypes", roomTypes.findAll());
		return "roomtypes/index";
	}
	
	// GET  - the list of roomtypes from a given hotel
	@RequestMapping(value = "/hotels/{hotelid}/roomtypes/", method = RequestMethod.GET)
	public String indexByHotel(@PathVariable("hotelid") long hotelID, Model model) {
		model.addAttribute("roomtypes", roomTypes.findByHotelId(hotelID));
//		model.addAttribute("url", "/hotels/"+hotelID+"/roomtypes/");
		return "roomtypes/index";
	}

	@RequestMapping(value = {"/roomtypes/{roomtypeid}","/hotels/{hotelid}/roomtypes/{roomtypeid}"}, method = RequestMethod.GET)
	public String showRoomTypeFromHotel(@PathVariable("roomtypeid") long roomTypeID, Model model) {
//		model.addAttribute("url", "/hotels/"+hotelID+"/roomtypes/");
		model.addAttribute("roomtype", roomTypes.findOne(roomTypeID));
		return "roomtypes/show";
	}

	@RequestMapping(value = {"/hotels/{hotelid}/roomtypes/new"}, method = RequestMethod.GET)
	public String newRoomTypeToHotel(@PathVariable("hotelid") long hotelID, Model model) {
		RoomType roomType = new RoomType();
		roomType.setHotel(hotels.findOne(hotelID));
		
		model.addAttribute("roomtype", roomType);
		model.addAttribute("url", "/hotels/"+hotelID+"/roomtypes/new");
		
//		model.addAttribute("roomtypes", roomTypes.findByHotelId(hotelID));
		return "roomtypes/create";
	}
	
	@RequestMapping(value = {"/hotels/{hotelid}/roomtypes/new"}, method = RequestMethod.POST)
	public String newRoomTypeToHotel(@PathVariable("hotelid") long hotelID, RoomType roomType, Model model) {
		roomTypes.save(roomType);
		return "roomtypes/show"; //TODO
	}
	

	@RequestMapping(value = {"/roomtypes/{roomtypeid}/edit","/hotels/{hotelid}/roomtypes/{roomtypeid}/edit"}, method = RequestMethod.GET)
	public String editRoomTypeFromHotel(@PathVariable("hotelid") long hotelID,
			@PathVariable("roomtypeid") long roomTypeID, Model model) {
		model.addAttribute("roomtype", roomTypes.findOne(roomTypeID));
		return "roomtypes/edit";
	}

	@RequestMapping(value = {"/roomtypes/{roomtypeid}/edit","/hotels/{hotelid}/roomtypes/{roomtypeid}/edit"}, method = RequestMethod.POST)
	public String indexRoomTypeToHotel(@PathVariable("hotelid") long hotelID,
			@PathVariable("roomtypeid") long roomTypeID, RoomType roomType, Model model) {
		roomTypes.save(roomType);
		return "roomtypes/show"; //TODO
	}

}