package app.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import app.model.Hotel;
import app.model.Photo;
import app.repository.HotelRepository;
import app.repository.PhotoRepository;
import app.util.HotelNotFoundException;

@Controller
//@RequestMapping(value="/hotels/{hotelid}/photos")
public class PhotoController {
	
	
	@Autowired
	HotelRepository hotels;
	
	@Autowired
	PhotoRepository photos;
	
	
	// GET  /hotels/{id}/photos  - the list all the photos to Hotel with Id
    @RequestMapping(value="/hotels/{hotelid}/photos", method=RequestMethod.GET)
    public String index(@PathVariable("hotelid") long hotelid, Model model) {
    	System.out.println("GET  /hotels/{id="+hotelid+"}/photos"); //TODO APAGAR
    	
    	Hotel hotel = hotels.findOne(hotelid);
    	model.addAttribute("hotelId", hotelid); 
    	model.addAttribute("photos", hotel.getPhotos());
        
    	return "photos/index";
    }
    
	// GET  /photos			- the list all the photos
    @RequestMapping(value="/photos", method=RequestMethod.GET)
    public String listAllPhotos(Model model) {
    	System.out.println("GET  /photos"); //TODO APAGAR

    	model.addAttribute("photos", photos.findAll());
    	return "photos/all";
    }
    
 // GET  /hotels/{hotelid}/photos/{photoid}/remove 	- removes the photo with identifier {id}
    @RequestMapping(value="{/hotels/{hotelid}/photos/{photoid}/remove", method=RequestMethod.GET)
    public String removePhoto(@PathVariable("hotelid") long hotelid, @PathVariable("photoid") long photoid, Model model) {
    	System.out.println("GET /hotels/{hotelid="+hotelid+"}/photos/{photoid="+photoid+"}/remove"); // TODO APAGAR 
    	
    	Photo photo = photos.findOne(photoid);
    	if( photo == null )
    		throw new HotelNotFoundException();
    	
    	Hotel hotel = hotels.findOne(hotelid);
    	hotel.getPhotos().remove(photo);
    	hotels.save(hotel);
    	
    	return "redirect:/hotels/{hotelid}";
    }
	
    // GET  /hotels/{id}/photos/new			- the form to fill the data for a new photo
    @RequestMapping(value="/hotels/{hotelid}/photos/new", method=RequestMethod.GET)
    public String newPhoto(@PathVariable("hotelid") long hotelid, Model model) {
    	System.out.println("GET  /hotels/{id="+hotelid+"}/photo/new"); // TODO APAGAR
    	
    	model.addAttribute("url", "/hotels/"+hotelid+"/photos/new");
//    	Photo photo = new Photo();
//    	System.out.println("NEW PHOTO ID : " + photo.getId()); // TODO APAGAR
    	//model.addAttribute("photo", photo);
    	return "photos/form";
    }
    
    // POST /hotels/{id}/photos/new         	- creates a new photo
    @RequestMapping(value="/hotels/{hotelid}/photos/new", method=RequestMethod.POST)
    public String savePhoto(@PathVariable("hotelid") long hotelid, @RequestParam("file") MultipartFile file, Model model) {
    	System.out.println("POST /hotels/{id="+hotelid+"}/photo/new"); // TODO APAGAR 
    	
    	if (!file.isEmpty()) {
            try {
            	
            	Photo photo = new Photo();
            	String photoId = Long.toString(photo.getId());
//            	System.out.println("NEW PHOTO ID : " + photo.getId()); // TODO APAGAR
            	String photoFilePath = "/src/main/resources/public/hotelphotos/" + photoId + ".jpeg";
            	String photoPath = "/hotelphotos/" + photoId + ".jpeg";
            	
                byte[] bytes = file.getBytes();
                File photoFile = new File( "." , photoFilePath);
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(photoFile));
                stream.write(bytes);
                stream.close();

                photo.setPath(photoPath);
                photo.setType("official");
                
            	Hotel hotel = hotels.findOne(hotelid);
//            	photo.setHotel(hotel);
            	hotel.getPhotos().add(photo);
            	//photos.save(photo);
            	hotels.save(hotel);
            	model.addAttribute("photo", photo);
                
                System.out.println("You successfully uploaded " + photo.getId() + "!");
            } catch (Exception e) {
            	System.out.println(e.getMessage());
                //return "You failed to upload " + name + " => " + e.getMessage();
            }
        } else {
        	System.out.println("You failed to upload because the file was empty.");
            //return "You failed to upload " + name + " because the file was empty.";
        }

    	return "redirect:/hotels/{hotelid}";
    }

}
